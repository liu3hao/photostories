photostories
============
This strings together a series of photos with accompany text into a very basic comic book style layout. Currently, it's a very basic version, so improve it in anyway you want!

* Requires jquery to work

Example
============
```
//this is the data to be used in the player
var PSData=new Array();
PSData.push({guid:<link to image>,title:<description of image>});

PSData.push({guid:<link to image>,title:<description of image>});

PSData.push({guid:<link to image>,title:<description of image>});

//creates a new photostory player using #PhotostoryHolder with the width 900 by 450
//loads the images from PSData
var PS1=new Photostory('#PhotostoryHolder',PSData,900,450);

//sets the directory for player ui
PS1.SetImgDirectory("ps/ps-ui");

//inits the player
PS1.Init();

//sets the display title for the series of images
PS1.SetDisplayTitle("Hello World!");

//sets the description for the series of images
PS1.SetDisplayDescp("This is just a sample!");

//sets the duration each photo is displayed in auto-play mode
PS1.SetEachTimePhoto(1000);
```