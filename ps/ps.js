// 2012 liu3hao
//version 1.01
function Photostory(root,data,width,height){
	//width and the height specified should include the pscontrols
	this.Container=root;
	this.root=root;
	this.rroot=root;
	
	this.displaytitle="";
	/*data[0][0][0];
	if(data[0][1][0]!=null){
		this.displaydescp=data[0][1][0];
	}else{
		this.displaydescp="";
	}*/
	this.displaypadding=5;
	this.data=data;
	this.cwidth=width;this.cheight=height;
	
	this.rwidth=width;this.rheight=height;
	
	this.padding=10;
	this.targetleft=0;
	this.totalslides=0;
	
	this.SetPadding=function(padding){this.padding=padding;}
	this.Rand=function(vmin,vmax){return Math.floor((vmax-vmin+1)*Math.random())+vmin}
	
	this.pscontrolsheight=25;
	this.navibarwidth=500;
	this.navibarheight=10;
	
	this.autoplaying=false;
	this.autoplayinginterval=0;
	
	this.picslidereference=new Array();
	this.numpics=this.data.length;
	
	this.navipopupwidth=50;
	this.navipopupheight=50;
	this.navipadding=3;
	
	this.naviballradius=14;
	this.mousestate=0;//0-up,1-down
	
	this.timeeachslide=700;//700;//seconds (for each photo, not slide)
	this.currentslide=1;
	
	this.layoutmap=new Array();
	this.relayout=true;
	this.imgdirectory="ps/ps-ui";
	
	this.overviewimagewidth=0;
	this.overviewimageheight=0;
	this.overviewimagepadding=5;
	this.overviewimagespacing=10;
	this.overviewimageinnerpadding=5;
	
	this.slidetransitiontime=500;
	
	this.SetImgDirectory=function(str){this.imgdirectory=str;}
	
	this.SetEachTimePhoto=function(times){
		if(times!=null){
			this.timeeachslide=times;
		}
	}
	
	this.Init=function(){
		this.Make();
		this.Layout();
		this.SetupHints();
	}
	
	this.SetupHints=function(){
		var root=this.root;
		$(root).find('#PlayButton').after("<div id='PlayButtonHint' class='Hints'>Play Photostory</div>");
		$(root).find('#PauseButton').after("<div id='PauseButtonHint' class='Hints' style='display:none'>Pause</div>");
		$(root).find('#ReplayButton').after("<div id='ReplayButtonHint' class='Hints' style='display:none'>Replay</div>");
		//$(root).find('#PlayButtonHint')
		
		$(root).find('#FullScreenButton').after("<div id='FullScreenButtonHint' class='Hints'>View in Full screen</div>");
		$(root).find('#FullScreenButtonHint').css('right',0);
		
		$(root).find('#CloseFullScreenButton').after("<div id='CloseFullScreenButtonHint' class='Hints' style='display:none'>Close Full screen</div>");
		$(root).find('#CloseFullScreenButton').css('right',0);
		
		$(root).find('#ShowOverviewButton').after("<div id='ShowOverviewButtonHint' class='Hints'>Show overview</div>");
		$(root).find('#ShowOverviewButtonHint').css('right',0);
	}
	
	this.Make=function(){
		//creates the psitems to hold the pics
		var num=this.data.length;
		var output="";
		var root=this.root;
		for(var i=0;i<num;i++){
			output+=this.MakePSItem(i,this.data[i]);
		}
		output+="<div class='PSTitle'><div class='Padder'><div class='Title'>"+this.displaytitle+"</div><div class='Description'>"+this.displaydescp+"</div></div></div>";
		
		$(root).html("<div class='PSWrapper'><div class='PSOverviewLayer' style='position:absolute;z-index:3'></div><div class='PSControlsLayer' style='position:absolute;z-index:4;'><a href='javascript:void(0)' id='PSNextArea'><div class='PSNaviShowBox'>next</div></a><a href='javascript:void(0)'  id='PSPrevArea' style='display:none'><div class='PSNaviShowBox'>prev</div></a><div id='NaviPopup'></div><div id='PSControlsBar'><div id='Liner'></div></div></div><div id='PSRootHolder' style='position:absolute;z-index:1;'><div id='PSHolder'>"+output+"</div></div></div>");
		
		$(root).find('.PSWrapper').css('width',this.cwidth).css('height',this.cheight);
		$(root).find('#PSRootHolder').css('overflow','hidden');
		$(root).find('.PSControlsLayer').css('width',1).css('height',1);
		
		this.MakeOverview();
		this.SetupOverview();
		
		$(this.root).find('.PSOverviewLayer').css('display','none');
		
		this.SetupNextPrevBar();
		this.SetupNaviPopup();
	}
	this.MakeOverview=function(){
		var output="";
		for(var i=0;i<this.numpics;i++){
			output+=this.MakeOverviewImageItem(i,this.data[i]);
		}
		$(this.root).find('.PSOverviewLayer').html("<div class='OverviewHeader'>Overview of "+this.displaytitle+"</div><div class='PSOverviewMasker'><div class='PSOverviewHolder'>"+output+"</div></div>");
		$(this.root).find('.PSOverviewLayer').css('opacity',0);
	}
	this.MakeOverviewImageItem=function(cnt,data){
		var img=data.guid;var descp=data.title;
		descp="<b>"+(cnt+1)+".</b> "+descp;
		return "<a href='"+img+"' style='position:absolute' class='PSOverviewClickable' title='"+descp+"'><div class='PSOverviewItem' style='position:relative'><div class='PSOverviewImgHolder'><img src='"+img+"' id='Overviewimg'></div><div class='Descp'>"+descp+"</div></div></a>";
	}
	this.SetupOverview=function(){
		var root=this.root;
		var padding=this.overviewimagepadding;
		var imgspacing=this.overviewimagespacing;
		var height=(this.cheight-this.pscontrolsheight)*0.8;
		var width=(height)*(16/9);
		
		$(root).find('.PSOverviewImgHolder').css('width',width).css('height',height).css('position','relative');
		$(root).find('.PSOverviewItem').find('.Descp').css('top',padding).css('left',padding);
		$(root).find('.PSOverviewItem').css('width',width).css('height',height);
		
		$(root).find('.PSOverviewClickable').css('width','').css('height','');
		$(root).find('.PSOverviewClickable').css('width',1).css('height',1);
		
		var frame;var imgw;var imgh;var imgratio;
		//force the height to be fixed, resize widthwise		
		
		var tmpoverviewimgwidth=width-padding*2;
		var tmpoverviewimgheight=height-padding*2;
		for(var i=0;i<this.numpics;i++){
			frame=$(root).find('.PSOverviewImgHolder')[i];
			
			this.PSResize(frame,tmpoverviewimgwidth,tmpoverviewimgheight);
			//alert(":"+width-padding*2+" "+height-padding*2);
			//alert("!"+this.cheight+"!");
			$(frame).css('left',padding).css('top',padding);
			$($(root).find('.PSOverviewItem')[i]).css('left',(width+imgspacing)*i);
		}	
		
		$(root).find('.PSOverviewLayer').css('position','absolute').css('width',this.cwidth).css('height',this.cheight-this.pscontrolsheight).css('background','url('+this.imgdirectory+"/bg.png)");
		var topheight=(this.cheight-this.pscontrolsheight)/2-height/2;
		$(root).find('.OverviewHeader').css('top',topheight-20).css('left',padding);
		
		$(root).find('.PSOverviewMasker').css('position','relative').css('top',topheight).css('width',this.cwidth-this.overviewimageinnerpadding*2).css('height',height+16+3).css('overflow-x','scroll').css('overflow-y','hidden').css('left',this.overviewimageinnerpadding);
	}
	this.ShowButton=function(str){
		var root=this.root;
		$(root).find('#PlayButton').css('display','none');
		$(root).find('#PlayButtonHint').css('display','none');
		$(root).find('#PauseButton').css('display','none');
		$(root).find('#PauseButtonHint').css('display','none');
		$(root).find('#ReplayButton').css('display','none');
		$(root).find('#ReplayButtonHint').css('display','none');
		switch(str){
			case 'play':
				$(root).find('#PlayButton').css('display','');
				$(root).find('#PlayButtonHint').css('display','');
				break;
			case 'pause':
				$(root).find('#PauseButton').css('display','');
				$(root).find('#PauseButtonHint').css('display','');
				break;
			case 'replay':
				$(root).find('#ReplayButton').css('display','');
				$(root).find('#ReplayButtonHint').css('display','');
				break;
			default:
				$(root).find('#PlayButton').css('display','');
				$(root).find('#PlayButtonHint').css('display','');
		}
	}
	this.ShowScreenModeButton=function(str){
		var root=this.root;
		$(root).find('#FullScreenButton').css('display','none');
		$(root).find('#FullScreenButtonHint').css('display','none');
		$(root).find('#CloseFullScreenButton').css('display','none');
		$(root).find('#CloseFullScreenButtonHint').css('display','none');
		switch(str){
			case 'full':$(root).find('#FullScreenButton').css('display','');$(root).find('#FullScreenButtonHint').css('display','');break;
			case 'close':$(root).find('#CloseFullScreenButton').css('display','');	$(root).find('#CloseFullScreenButtonHint').css('display','');break;
			default:$(root).find('#FullScreenButton').css('display','');$(root).find('#FullScreenButtonHint').css('display','');
		}
	}
	this.LayoutNaviItems=function(){
		var root=this.root;
		var innerpadding=this.navipadding;
		
		$(root).find('#NaviPopupMain').css('width',this.navipopupwidth).css('height',this.navipopupheight).css('overflow','hidden');
		
		$(root).find('#NaviPopupFrame').css('border','outer thin #000').css('padding',innerpadding).css('position','relative').css('top',this.cheight-this.pscontrolsheight-this.navipopupheight-this.navipadding*2-3).css('width',this.navipopupwidth);
		
		$(root).find('.PSNaviPopup').css('width',this.navipopupwidth).css('height',this.navipopupheight).css('overflow','hidden');
		
		$(root).find('#NaviPopupHolder').css('position','relative');
		
		for(var i=0;i<this.numpics;i++){
			this.PSResize($(root).find('.PSNaviPopup')[i],this.navipopupwidth,this.navipopupheight);
			$($(root).find('.PSNaviPopup')[i]).css('left',this.navipopupwidth*i);
		}
		$('.PSNaviPopupFrame').css('width',0).css('height',0);
		$(root).find('#NaviPopupFrame').css('opacity',0);
	}
	this.SetupNaviPopup=function(){
		var root=this.root;
		var num=this.data.length;
		var output="";var img;
		for(var i=0;i<num;i++){output+=this.MakeNaviPopupItem(this.data[i]);}
		$(root).find('#NaviPopup').html("<div id='NaviPopupFrame'><div id='NaviPopupMain'><div id='NaviPopupHolder'>"+output+"</div></div></div>");
		this.LayoutNaviItems();
		this.SetupNavibarActions();
	}
	this.SetupNavibarActions=function(){
		var root=this.root;
		//NAVIBAR ACTIONS
		var ma=this;
		$(root).find('#NaviBar').mousemove(function(event){
			var mx=event.clientX;
			var rootx=$(root).find('#NaviBar').offset().left;
			var pic=Math.ceil(((mx-rootx)/ma.navibarwidth)*ma.numpics);
			ma.ShowNaviPic(pic,mx-rootx);
			if(ma.mousestate==1){
				ma.MoveNaviBallToPosition(mx-rootx);
			}
			$(root).css('cursor','default');
			event.stopPropagation();
		}).mouseleave(function(event){
			var root=ma.root;
			$(root).find('#NaviPopupFrame').animate({'opacity':0},200);
			ma.DisplayNone($(root).find('#NaviPopupFrame')[0],201);
			if(ma.mousestate==1){
				
				var mx=event.clientX;
				var rootx=$(root).find('#PSControlsBar').find('#NaviBar').offset().left;
				var pic=Math.ceil(((mx-rootx)/ma.navibarwidth)*ma.numpics);
				ma.MoveNaviBallToPosition(mx-rootx);
				ma.GotoPic(pic);
			}
			ma.mousestate=0;
		}).mouseenter(function(event){
			$(root).find('#NaviPopupFrame').css('display','').css('opacity',0).animate({'opacity':1},200);
		});
		$(root).find('#NaviBar').mousedown(function(event){
			ma.mousestate=1;
			$(root).css('cursor','default');
			event.stopPropagation();
			ma.ShowButton('play');
		}).mouseup(function(event){
			ma.mousestate=0;
			var root=ma.root;
			var mx=event.clientX;
			var rootx=$(root).find('#PSControlsBar').find('#NaviBar').offset().left;
			var pic=Math.ceil(((mx-rootx)/ma.navibarwidth)*ma.numpics);
			ma.MoveNaviBallToPosition(mx-rootx);
			ma.GotoPic(pic);
		});
		
		var ma=this;
		$(root).find('#PSControlsBar').find('#NaviBar').click(function(event){
			var mx=event.clientX;
			var rootx=$(root).find('#PSControlsBar').find('#NaviBar').offset().left;
			var pic=Math.ceil(((mx-rootx)/ma.navibarwidth)*ma.numpics);
			ma.MoveNaviBallToPosition(mx-rootx);
			ma.GotoPic(pic);
			if(ma.autoplaying){
				ma.StopAutoPlay();
				ma.AutoPlay();
			}else{
				ma.ShowButton('play');
			}
		});
	}
	this.DisplayNone=function(obj,time){setTimeout(function(){$(obj).css("display","none");},time);}
	this.ShowNaviPic=function(id,frameleft){
		var root=this.root;
		if(id>0 && id <this.numpics+1){
			$(root).find('#NaviPopupHolder').css('left',-(id-1)*this.navipopupwidth);
			$(root).find('#NaviPopupFrame').css('left',frameleft);
		}else{
			$(root).find('#NaviPopupFrame').css('display','none');
		}
	}
	this.MakeNaviPopupItem=function(data){
		var img=data.guid;
		var output="<div class='PSNaviPopupFrame'><div class='PSNaviPopup' style='position:relative'><img src='"+img+"'/></div></div>";
		return output;
	}
	this.SetupNextPrevBar=function(){
		var root=this.root;
		var ratio=0.1;var initialopacity=0;
		var h1=this.cheight-this.pscontrolsheight;
		var w1=this.cwidth*ratio;
		$(root).find('#PSNextArea').css('position','absolute').css('left',this.cwidth-w1).css('width',w1).css('height',h1).css('background','none');
		$(root).find('#PSNextArea').css('text-decoration','none');
		
		$(root).find('#PSNextArea').find('div').css('width',w1*0.5-1).css('padding-top',5).css('padding-bottom',5).css('background','#fff').css('position','relative').css('top',h1*0.15).css('text-align','center').css('right',-w1+w1*0.5).css('border-radius','3px 0 0 3px');
		
		$(root).find('#PSPrevArea').css('position','absolute').css('left',0).css('width',w1).css('height',h1).css('background','none');
		$(root).find('#PSPrevArea').css('text-decoration','none');
		
		$(root).find('#PSPrevArea').find('div').css('width',w1*0.5-1).css('padding-top',5).css('padding-bottom',5).css('background','#fff').css('position','relative').css('top',h1*0.15).css('text-align','center').css('border-radius','0 3px 3px 0');
		
		this.SetupNextPrevActions();
		
		$(root).find('#PSControlsBar').css('position','absolute').css('top',this.cheight-this.pscontrolsheight).css('height',this.pscontrolsheight).css('width',this.cwidth);
		$(root).find('#PSControlsBar').find('#Liner').css('width',this.cwidth).css('border-bottom','solid thin #777');
		
		$(root).find('#PSControlsBar').html("<a href='javascript:void(0)' id='ActionButton'><img src='"+this.imgdirectory+"/play.png' id='PlayButton'/><img src='"+this.imgdirectory+"/pause.png' style='display:none' id='PauseButton'/><img src='"+this.imgdirectory+"/replay.png' style='display:none' id='ReplayButton'/></a><div class='Split'></div><div id='NaviBar' style='position:relative'><div id='NaviBarBall'></div></div><div class='Split'></div><div id='PSDetails'>"+this.numpics+" images</div><div class='Split'></div><a href='javascript:void(0)' id='ScreenModeButton'><img src='"+this.imgdirectory+"/fullscreen.png' id='FullScreenButton'/><img src='"+this.imgdirectory+"/exitfullscreen.png' id='CloseFullScreenButton' style='display:none'/></a><div class='Split'></div><a href='javascript:void(0)' id='ShowOverviewButtonHolder'><img src='"+this.imgdirectory+"/all.png' id='ShowOverviewButton'/></a>");		
		$(root).find('#PSControlsBar').find('.Split').css('height',this.pscontrolsheight);
		
		//alert(this.Spx($('#PSDetails').css('width')));
		this.navibarwidth=(this.cwidth-3*25-8-$('#PSDetails').outerWidth());
		
		$(root).find('#PSControlsBar').find('#NaviBar').css('width',this.navibarwidth).css('height',this.navibarheight);
		
		$(root).find('#NaviBarBall').css('position','absolute').css('border-radius','30px').css('width',this.naviballradius).css('height',this.naviballradius).css('top',-2).css('left',0);
		
		this.SetupActionButtonsActions();
	}
	this.SetupNextPrevActions=function(){
		var ma=this;
		var root=this.root;
		$(root).find('#PSNextArea').mouseenter(function(){
			//$(root).find('#PSNextArea').animate({'opacity':0.4},200);
		}).mouseleave(function(){
			//$(root).find('#PSNextArea').animate({'opacity':0},200);
		}).click(function(){
			ma.PSNext();
		});
		$(root).find('#PSPrevArea').mouseenter(function(){
		//	$(root).find('#PSPrevArea').animate({'opacity':0.4},200);
		}).mouseleave(function(){
			//$(root).find('#PSPrevArea').animate({'opacity':0},200);
		}).click(function(){
			ma.PSPrev();
		});
	}
	this.SetupActionButtonsActions=function(){
		var ma=this;
		var root=this.root;
		//ACTION BUTTONS
		$(root).find('#PlayButton').bind('click',function(event){
			if(ma.LeftClick(event)){ma.AutoPlay();}
		});
		$(root).find('#ReplayButton').bind('click',function(event){
			if(ma.LeftClick(event)){ma.Replay();}
		});
		$(root).find('#PauseButton').bind('click',function(event){
			if(ma.LeftClick(event)){ma.StopAutoPlay();}
		});
		
		$(root).find('#FullScreenButton').bind('click',function(event){
			if(ma.LeftClick(event)){
				ma.FullScreenMode();
			}
		});
		$(root).find('#CloseFullScreenButton').bind('click',function(event){
			if(ma.LeftClick(event)){
				ma.CloseFullScreenMode();
			}
		});
		$(root).find('#ShowOverviewButton').unbind('click');
		if($(root).find('.PSOverviewLayer').css('opacity')!=1){
			$(root).find('#ShowOverviewButton').bind('click',function(event){
				if(ma.LeftClick(event)){ma.ShowOverview();event.stopPropagation();}
			});
		}else{
			$(root).find('#ShowOverviewButton').bind('click',function(event){
				if(ma.LeftClick(event)){ma.CloseShowOverview();event.stopPropagation();}
			});
		}
	}
	this.MoveNaviBallToPosition=function(left){
		var finalleft=left;
		var root=this.root;
		if(finalleft>this.naviballradius/2 && finalleft<this.navibarwidth-this.naviballradius/2){$(root).find('#NaviBarBall').css('left',finalleft-this.naviballradius/2);}
	}
	this.PSNext=function(){
		var root=this.root;
		if(this.targetleft>-(this.totalslides-1)*this.cwidth){			
			this.targetleft-=this.cwidth;
			$(root).find('#PSHolder').animate({'left':this.targetleft},this.slidetransitiontime);
			this.currentslide++;
			if(!this.autoplaying){
				this.NaviBarUpdatePositionSlideTo();
			}else{
				this.NaviBarAutoplay();
			}
			
			if(!this.autoplaying){
				if(this.currentslide==this.totalslides){
					$(root).find('#PSNextArea').animate({'opacity':0},400);
					this.DisplayNone($(root).find('#PSNextArea')[0],401);
				}else{
					if($(root).find('#PSNextArea').css('display')=='none'){
						$(root).find('#PSNextArea').css('display','').css('opacity',0).animate({'opacity':1},400);
					}
					if($(root).find('#PSPrevArea').css('display')=='none'){
						$(root).find('#PSPrevArea').css('display','').css('opacity',0).animate({'opacity':1},400);
					}
				}
			}
			return true;
		}else{
			$(this.root).find('#NaviBarBall').animate({'left':this.navibarwidth-this.naviballradius},200);		
			var ma=this;
			setTimeout(function(){
				ma.ShowButton('replay');
				ma.autoplaying=false;
				$(ma.root).find('#PSPrevArea').css('display','').css('opacity',0).animate({'opacity':1},400);
								},201);
		}
		
		if(this.autoplaying){this.StopAutoPlay();}
		return false;
	}
	this.NaviBarUpdatePositionSlideTo=function(){
		var n=this.picslidereference.length;
		var pic=0;
		for(var i=0;i<n;i++){if(Number(this.picslidereference[i])==this.currentslide){pic=i;break;}}
		$(this.root).find('#NaviBarBall').animate({'left':pic/this.numpics*this.navibarwidth},200);
	}
	this.NaviBarAutoplay=function(){
		var time=this.PicsInSlide(this.currentslide)*this.timeeachslide;
		for(var i=0;i<this.numpics;i++){
			if(Number(this.picslidereference[i])==this.currentslide){
				pic=i;break;
			}
		}
		pic+=this.PicsInSlide(this.currentslide);
		if(this.currentslide>=this.totalslides){
			$(this.root).find('#NaviBarBall').animate({'left':this.navibarwidth-this.naviballradius},time,'linear');
			var ma=this;
			setTimeout(function(){
				ma.ShowButton('replay');
				ma.autoplaying=false;
				$(ma.root).find('#PSPrevArea').css('display','').css('opacity',0).animate({'opacity':1},400);
			},time);
		}else{
			$(this.root).find('#NaviBarBall').animate({'left':pic/this.numpics*this.navibarwidth},time,"linear");
		}
	}
	this.LeftClick=function(event){return (event.button==0)?true:false;}
	this.PSPrev=function(){
		var root=this.root;
		if(this.targetleft<0){
			this.targetleft+=this.cwidth;
			$(root).find('#PSHolder').animate({'left':this.targetleft},this.slidetransitiontime);
			this.currentslide--;
			if(!this.autoplaying){
				this.NaviBarUpdatePositionSlideTo();
			}
			
			if(this.currentslide==0){
				$(root).find('#PSPrevArea').animate({'opacity':0},400);
				this.DisplayNone($(root).find('#PSPrevArea')[0],401);
			}else{
				if(!this.autoplaying){
					if($(root).find('#PSPrevArea').css('display')=='none'){
						$(root).find('#PSPrevArea').css('display','').css('opacity',0).animate({'opacity':1},400);
					}
					if($(root).find('#PSNextArea').css('display')=='none'){
						$(root).find('#PSNextArea').css('display','').css('opacity',0).animate({'opacity':1},400);
					}
				}
			}
			
			return true;
		}
		if(this.autoplaying){
			this.StopAutoPlay();
		}
		return false;
	}
	this.StopAutoPlay=function(){
		this.autoplaying=false;
		$(this.root).find('#NaviBarBall').stop();
		clearInterval(this.autoplayinginterval);
		this.ShowButton('play');
		
		var root=this.root;
		if($(root).find('#PSNextArea').css('display')=='none'){
			$(root).find('#PSNextArea').css('display','').css('opacity',0).animate({'opacity':1},400);
		}
		if($(root).find('#PSPrevArea').css('display')=='none'){
			$(root).find('#PSPrevArea').css('display','').css('opacity',0).animate({'opacity':1},400);
		}
		
	}
	this.AutoPlay=function(){
		this.CloseShowOverview();
		var ma=this;		
		var ballLeft=this.Spx($(this.root).find('#NaviBarBall').css('left'));
		
		var root=this.root;
		$(root).find('#PSNextArea').animate({'opacity':0},400);
		$(root).find('#PSPrevArea').animate({'opacity':0},400);
		
		this.DisplayNone($(root).find('#PSNextArea')[0],401);
		this.DisplayNone($(root).find('#PSPrevArea')[0],401);
		
		if(ballLeft==0){
			ma.autoplayinginterval=setInterval(
				function(){
					clearInterval(ma.autoplayinginterval);
					ma.AutoPlayStep();
				},this.timeeachslide*this.PicsInSlide(this.currentslide));
			this.NaviBarAutoplay();
		}else{
			//calculate the time left for the slide
			var last=0;
			var lastslidepic=0;
			
			var timelinepic=Math.ceil(ballLeft/this.navibarwidth*this.numpics);
			this.currentslide=this.picslidereference[timelinepic-1];
			this.targetleft=(this.currentslide-1)*-(this.cwidth);
			
			for(var i=0;i<this.numpics;i++){
				if(this.picslidereference[i]==this.currentslide){
					last=i;
				}else if(last==0){
					lastslidepic=i;
				}
			}
			var dif=last-ballLeft/this.navibarwidth*this.numpics;
			ma.autoplayinginterval=setInterval(
			function(){
				clearInterval(ma.autoplayinginterval);
				ma.AutoPlayStep();
			},this.timeeachslide*dif);
			
			var endslide=(last+1)/this.numpics*this.navibarwidth-this.naviballradius;
			$(this.root).find('#NaviBarBall').animate({'left':endslide},this.timeeachslide*dif,"linear");
		}
		
		this.autoplaying=true;
		ma.ShowButton('pause');
	}
	this.AutoPlayStep=function(){
		this.PSNext();
		var ma=this;
		if(this.currentslide<this.totalslides && this.autoplaying){
			this.autoplayinginterval=setInterval(function(){
				clearInterval(ma.autoplayinginterval);
				ma.AutoPlayStep();
			},this.timeeachslide*this.PicsInSlide(this.currentslide));
		}
	}
	this.Replay=function(){
		this.targetleft=0;
		this.currentslide=1;
		$(this.root).find('#PSHolder').animate({'left':0},200);
		$(this.root).find('#NaviBarBall').animate({'left':0},200);
		this.ShowButton('play');
		var ma=this;
		setTimeout(function(){ma.AutoPlay();},400);
	}
	this.MakePSItem=function(cnt,itemdata){
		var img=itemdata.guid;
		console.log(itemdata);
		var descp=itemdata.title;
		cnt+=1;
		return "<a rel='lightbox' href='"+img+"' class='PSItemClickable' title='"+descp+"'><div class='PSItem'><div class='SubPS1'><div class='ImgHolder'><img src='"+img+"' style='position:relative;' id='PS_IMG"+cnt+"' /></div><div class='Descp' style='position:absolute;'><b>"+cnt+"</b>. "+descp+"</div></div></div></a>";
	}
	this.Layout=function(){
		var root=this.root;
		this.currentslide=1;
		
		this.picslidereference=new Array();
		var containerwidth=this.cwidth;
		
		var containerheight=this.cheight-this.pscontrolsheight;
		$(root).find('#PSRootHolder');
		$(root).find('#PSRootHolder').css('width',this.cwidth).css('height',this.cheight);
		
		$(root).find('.PSItemClickable').css('position','absolute');
		$(root).find('#PSHolder').css("position","relative").css("top",0).css('left',0);
		this.targetleft=0;
		
		$(root).find('.PSItem').css('position','relative');
		//get sizes of big and small images
		var img1w=containerwidth-padding*2;var img1h=containerheight-padding*2;//the larger images
		var img2w;var img2h;//the smaller images
		
		var groupsize=1;//var groupcnt=0;
		var layoutmode=4;
		
		var insertop=0;
		var insertleft=0;
		
		var dochild=false;
		var layoutcnt=0;
		var GeneratedLayoutMap=[];
		var layoutmap=[];
		var groupcnt=0;
		
		var totalslides=1;
		
		$(root).find('.PSItem').css('border','solid thin #222');
		
		var padding=this.padding;
		//$(root).find('.PSItem .Descp').css('top',padding);
		var num=$(root).find(".PSItem").length;
		this.numpics=num;
		var psitem;
		
		//position the title!		
		$(root).find('.PSTitle').find('.Padder').css('padding',this.displaypadding);
		if(Math.random()<0.5){
			$(root).find('.PSTitle').css('right',padding).css('text-align','right').css('top',padding+(this.cheight-padding*2)*0.5);
		}else{
			$(root).find('.PSTitle').css('left',padding).css('text-align','left').css('top',padding+(this.cheight-padding*2)*0.5);
		}
		
		this.layoutmap.push([layoutmode,groupsize]);
		for(var i=0;i<num;i++){
			psitem=$(root).find('.PSItem')[i];
			if(groupcnt==0 || groupcnt>=groupsize){
				if(dochild){
					insertleft+=containerwidth;
					dochild=false;
					groupcnt=0;
					
					layoutcnt++;
					totalslides++;
					
					if(layoutmap[layoutcnt]!=null){
						layoutmode=layoutmap[layoutcnt][0];
						groupsize=layoutmap[layoutcnt][1];
					}else{
						layoutmode=this.Rand(1,4);
						if(layoutmode==4){
							groupsize=this.Rand(2,3);
						}else{
							groupsize=3;//this.Rand(3,4);
						}
					}
					//do perfect ending
					if((num-i)==3 || (num-i)==4){
						groupsize=3;
						layoutmode=this.Rand(1,4);
					}else if((num-i)==2){
						groupsize=(num-i);
						layoutmode=2;
					}else if((num-i)==1){
						groupsize=1;
						layoutmode=4;
					}
					
					this.layoutmap.push([layoutmode,groupsize]);
				}
				if(layoutmode==1){
					var ratio=0.5;
					img1w=containerwidth-padding*2;
					img1h=containerheight*ratio-padding;
					
					img2w=(containerwidth-padding*(groupsize))/(groupsize-1);
					img2h=containerheight-img1h-padding*3;
					
					this.PSResize(psitem,img1w,img1h);
					
					$(psitem).css('top',padding).css('left',insertleft+padding);					
				}else if(layoutmode==2 || layoutmode==3){
					var ratio=0.5;
					img1w=containerwidth*ratio-padding*2;
					img1h=containerheight-padding*2;
					
					img2w=containerwidth*(1-ratio)-padding;
					img2h=(containerheight-padding*(groupsize))/(groupsize-1);
					
					if(layoutmode==2){
						this.PSResize(psitem,img1w,img1h);
						$(psitem).css('top',padding).css('left',insertleft+padding);
					}else{
						this.PSResize(psitem,img2w,img2h,padding);
						$(psitem).css('left',insertleft+padding).css('top',padding);
					}				
				}else if(layoutmode==4){
					img1w=(containerwidth-padding*(groupsize+1))/groupsize;
					img1h=containerheight-padding*2;
					img2w=img1w;
					img2h=img1h;
					this.PSResize(psitem,img1w,img1h,padding);
					$(psitem).css("top",padding).css('left',insertleft+padding);
					if(groupsize==1){
						dochild=true;
					}
				}
				groupcnt++;
			}else{
				if(layoutmode==1){
					this.PSResize(psitem,img2w,img2h,padding);
					$(psitem).css("top",img1h+padding*2);
					$(psitem).css("left",insertleft+(img2w+padding)*(groupcnt-1)+padding);
				}else if(layoutmode==2){
					this.PSResize(psitem,img2w,img2h,padding);
					$(psitem).css("top",padding+(img2h+padding)*(groupcnt-1)).css('left',insertleft+img1w+padding*2);
				}else if(layoutmode==3){
					if(groupcnt+1==groupsize){
						this.PSResize(psitem,img1w,img1h,padding);
						$(psitem).css("top",padding).css("left",insertleft+padding*2+img2w);
					}else{
						this.PSResize(psitem,img2w,img2h,padding);
						$(psitem).css("top",padding+(img2h+padding)*(groupcnt)).css('left',insertleft+padding);
					}
				}else if(layoutmode==4){
					this.PSResize(psitem,img2w,img2h,padding);
					$(psitem).css("left",insertleft+padding+(img2w+padding)*(groupcnt)).css("top",padding);
				}
				dochild=true;
				groupcnt++;
			}
			$(psitem).find(".Descp").css(((Math.random()<0.5)?'top':'bottom'),0).css(((Math.random()<0.5)?'left':'right'),0);
			this.picslidereference.push(totalslides);
		}
		this.totalslides=totalslides;
		this.relayout=false;
	}
	this.GotoPic=function(id){
		var root=this.root;
		if(this.picslidereference[id-1]!=null){
			var slidenum=this.picslidereference[id-1];
			$(root).find('#PSHolder').animate({'left':-(slidenum-1)*this.cwidth},200);
			var ma=this;
			setTimeout(function(){
				ma.PicBlink(id);
			},300);
		}
	}
	this.PicBlink=function(id){
		var root=this.root;
		if($(root).find('#PS_IMG'+id)[0]!=null){$(root).find('#PS_IMG'+id).css('opacity',0.5).animate({'opacity':1},500);}
	}
	this.Spx=function(val){return Number(val.replace("px",""));}
	
	this.PSResize=function(frame,containerwidth,containerheight){
		//frame=holder of the img
		$(frame).css('overflow','hidden').css('width',containerwidth).css('height',containerheight);
		var obj=$(frame).find('img')[0];
		if(obj==null){
			return;
		}
		if(obj.complete){
			this.PSSubResize(obj,containerwidth,containerheight);
		}else{
			var ma=this;
			obj.onload=function(){ma.PSSubResize(this,containerwidth,containerheight);}
		}
	}
	this.PSSubResize=function(obj,containerwidth,containerheight){
		$(obj).css("width","").css("height","");
		var cratio=containerheight/containerwidth;
		
		var width=this.Spx($(obj).css('width'));
		var height=this.Spx($(obj).css('height'));
		var ratio;
		if(width=="0" && height=="0"){
			width=obj.width;
			height=obj.height;
			ratio=height/width;
		}else{
			ratio=height/width;
		}
		
		if(cratio>=ratio){
			$(obj).css('height',containerheight).css("width",containerheight/ratio);
			$(obj).css('position','relative').css('left',(containerheight/ratio-containerwidth)*-0.5);
		}else{
			$(obj).css("width",containerwidth).css("height",containerwidth*ratio);
			$(obj).css('position','relative').css('top',(containerwidth*ratio-containerheight)*-0.5);
		}
	}
	this.PicsInSlide=function(slidenum){
		var num=0;var flag=0;
		for(var i=0;i<this.numpics;i++){
			if(this.picslidereference[i]==slidenum){
				num++;
				flag=1;
			}else if(flag==1){
				break;
			}
		}
		return num;
	}
	this.FullScreenMode=function(){
		this.StopAutoPlay();
		
		var bodyobj=document.body;
		var rand=this.Rand(1000,9999);
		$(bodyobj).append("<div id='PSFullScreen"+rand+"'><div id='Container'></div></div>");
		var fsname="#PSFullScreen"+rand;
		var originalwidth=$(fsname).css('width');
		
		var hiderwidth=this.Spx(originalwidth);
		var hiderheight=document.height;
		$(fsname).css('position','absolute').css('z-index',0).css('top',0).css('left',0).css('background','url('+this.imgdirectory+'/bg.png)').css('width',hiderwidth).css('height',hiderheight);
		var root=this.root;
		
		$(fsname).css('background','#333');		
		$(fsname).css('opacity',0).animate({'opacity':1},200);
		
		$(fsname).find('#Container').html($(root).html()).css('opacity',0);
		$(root).html("");
		
		this.cwidth=hiderwidth*0.9;
		this.cheight=window.innerHeight*0.9;
		
		var screenleft=(hiderwidth-this.cwidth)/2;
		var screentop=(window.innerHeight-this.cheight)/2;
		$(fsname).find('#Container').css('background','#fff').css('width',this.cwidth).css('height',this.cheight).css('position','relative').css('left',screenleft).css('top',window.scrollY+screentop*0.7);
		
		this.root=$(fsname).find('#Container')[0];
		this.PositionStuff();
		this.SetupNextPrevBar();
		this.LayoutNaviItems();
		this.SetupNavibarActions();
		this.SetupOverview();
		
		this.ShowScreenModeButton('close');
		
		this.Layout();
		
		this.SetupHints();
		
		this.LightBoxBinder();
		setTimeout(function(){
			$(fsname).find('#Container').animate({'opacity':1},200);
		},300);
	}
	this.CloseFullScreenMode=function(){
		$($(this.root).parent()).remove();
		$(this.rroot).html($(this.root).html());
		$(this.root).html("");
		this.cwidth=this.rwidth;this.cheight=this.rheight;
		
		this.root=this.rroot;
		var root=this.root;
		
		$(root).css('opacity',0);
		this.PositionStuff();
		this.SetupNextPrevBar();
		this.LayoutNaviItems();
		this.SetupNavibarActions();
		this.SetupOverview();
		
		this.ShowScreenModeButton('full');
		this.Layout();
		
		$(root).animate({'opacity':1},200);
		
		this.SetupHints();
		this.LightBoxBinder();
	}
	this.PositionStuff=function(){
		var root=this.root;
		$(root).find('.PSWrapper').css('width',this.cwidth).css('height',this.cheight);
		$(root).find('#PSRootHolder').css('overflow','hidden');
		$(root).find('.PSControlsLayer').css('width',1).css('height',1);
		
		$(root).find('.PSWrapper').css('width',this.cwidth).css('height',this.cheight);
		$(root).find('.PSControlsLayer').css('width',1).css('height',1);
		
		$(root).find(".PSItemClickable").css("width",'auto').css("height",'auto');
		$(root).find(".PSItem").css("width","").css("height","").css("left",0).css("top",0);
		
		$(root).find(".PSItem").find("img").css("width","auto").css("height","auto").css("top",0).css("left",0);
		$(root).find(".PSItem").find(".ImgHolder").css("width","").css("height","")
		
		$(root).find('.PSItem').find('.Descp').css('top','').css('left','').css('right','').css('bottom','');
		$(root).find('.PSTitle').css('left','').css('right','').css('top','').css('bottom','');
	}
	this.SetDisplayTitle=function(str){
		this.displaytitle=str;
		$(this.root).find('.PSTitle').find('.Title').html(this.displaytitle);
	}
	this.SetDisplayDescp=function(str){
		this.displaydescp=str;
		$(this.root).find('.PSTitle').find('.Description').html(this.displaydescp);
	}
	this.ShowOverview=function(){
		var root=this.root;
		$(this.root).find('.PSOverviewLayer').css('display','').animate({'opacity':1},200);
		var ma=this;
		$(root).find('#ShowOverviewButtonHint').html('Hide overview');
		$(root).find('#ShowOverviewButton').unbind('click');
		$(root).find('#ShowOverviewButton').bind('click',function(event){
			if(ma.LeftClick(event)){
				ma.CloseShowOverview();
				event.stopPropagation();
			}
		});
	}
	this.CloseShowOverview=function(){
		var root=this.root;
		$(this.root).find('.PSOverviewLayer').animate({'opacity':0},200);
		this.DisplayNone($(this.root).find('.PSOverviewLayer')[0],201);
		var ma=this;
		$(root).find('#ShowOverviewButtonHint').html('Show overview');
		$(root).find('#ShowOverviewButton').unbind('click');
		$(root).find('#ShowOverviewButton').bind('click',function(event){
			if(ma.LeftClick(event)){
				ma.ShowOverview();
				event.stopPropagation();
			}
		});
	}
	
	this.BindLightBox;
	this.LightBoxBinder=function(){
		if(this.BindLightBox!=null){
			this.BindLightBox();	
		}
	}
	this.SetLightBoxBinder=function(func){
		this.BindLightBox=func;
		this.LightBoxBinder();
	}
	return this;
}